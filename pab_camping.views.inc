<?php

/**
 * Implements hook_views_data_alter().
 */
function pab_camping_views_data_alter(&$data) {
  $data['bat_bookings']['nights'] = array(
    'field' => array(
      'title' => t('Nights'),
      'help' => t('Provide number of nights.'),
      'handler' => 'pab_camping_handler_night_field',
    ),
  );
}
