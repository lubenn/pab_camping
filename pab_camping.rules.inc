<?php

/**
 * Implements hook_rules_action_info().
 */
function pab_camping_rules_action_info() {
  $actions = array(
    'pab_camping_adult_price' => array(
      'label' => t('Add adult pricing'),
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Line item'),
        ),
        'component_name' => array(
          'type' => 'text',
          'label' => t('Price component type'),
          'description' => t('Price components track changes to prices made during the price calculation process, and they are carried over from the unit price to the total price of a line item. When an order total is calculated, it combines all the components of every line item on the order. When the unit price is altered by this action, the selected type of price component will be added to its data array and reflected in the order total display when it is formatted with components showing. Defaults to base price, which displays as the order Subtotal.'),
          'options list' => 'commerce_line_item_price_component_options_list',
          'default value' => 'base_price',
        ),
        'round_mode' => array(
          'type' => 'integer',
          'label' => t('Price rounding mode'),
          'description' => t('Round the resulting price amount after performing this operation.'),
          'options list' => 'commerce_round_mode_options_list',
          'default value' => COMMERCE_ROUND_HALF_UP,
        ),
      ),
      'group' => t('Commerce Line Item'),
    ),
  );
  return $actions;
}

/**
 * Rules action: add an amount to the unit price.
 */
function pab_camping_adult_price($line_item, $component_name, $round_mode) {
  $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $unit_price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price', TRUE);

  $extra_adults = $wrapper->field_extra_adults->value();
  $total_nights = $wrapper->field_total_nights->value();
  $current_amount = $unit_price['amount'];

  if($extra_adults && isset($wrapper->commerce_product)) {
    $product = $wrapper->commerce_product->value();
    $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
    $price = $product_wrapper->field_price_per_adult->value();
    if($price > 0) {
      // extra adults * price * total nights
      $amount = ($extra_adults * $price['amount']) * $total_nights;
      $updated_amount = commerce_round($round_mode, $current_amount + $amount);

      $difference = array(
        'amount' => $updated_amount - $current_amount,
        'currency_code' => $unit_price['currency_code'],
        'data' => array(),
      );

      // Set the amount of the unit price and add the difference as a component.
      $wrapper->commerce_unit_price->amount = $updated_amount;

      $wrapper->commerce_unit_price->data = commerce_price_component_add(
        $wrapper->commerce_unit_price->value(),
        $component_name,
        $difference,
        TRUE
      );
    }
  }
}
