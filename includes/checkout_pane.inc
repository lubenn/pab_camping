<?php

/**
 * @file
 * Checkout pane callback functions for the checkout module.
 */

/**
 * Checkout pane callback: presents a completion message on the complete page.
 */
function pab_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane_form = array();

  foreach($order->commerce_line_items[LANGUAGE_NONE] as $key => $line_item) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item['line_item_id']);
    if($line_item_wrapper->type->value() != 'product'){
      continue;
    }
    $pane_form['arrival'] = array(
      '#title' => t('Arrival Date'),
      '#type' => 'textfield',
      '#value' => date('l, F d, Y', $line_item_wrapper->field_start_date->value()),
      '#disabled' => TRUE,
      '#prefix' => '<div class="form-group row"><div class="col-sm-6">',
      '#suffix' => '</div>',
      '#weight' => 0,
    );

    $pane_form['departure'] = array(
      '#title' => t('Departure Date'),
      '#type' => 'textfield',
      '#value' => date('l, F d, Y',$line_item_wrapper->field_end_date->value()),
      '#disabled' => TRUE,
      '#prefix' => '<div class="col-sm-6">',
      '#suffix' => '</div>',
      '#weight' => 1,
    );

    $form['checkout_pab']['adults'] = array(
      '#title' => t('Number of Adults'),
      '#type' => 'textfield',
      '#value' => $line_item_wrapper->field_number_of_adults->value(),
      '#disabled' => TRUE,
      '#prefix' => '<div class="col-sm-6">',
      '#suffix' => '</div>'
    );

    $pane_form['product'] = array(
      '#title' => t('Pitch'),
      '#type' => 'textfield',
      '#value' => $line_item_wrapper->commerce_product->title->value(),
      '#disabled' => TRUE,
      '#prefix' => '<div class="col-sm-6">',
      '#suffix' => '</div></div>',
      '#weight' => 4,
    );

    $price = field_view_field('commerce_order', $order, 'commerce_order_total');

    $pane_form['price'] = array(
      '#markup' => render($price),
      '#prefix' => '<div class="commerce-price">',
      '#suffix' => '</div>',
      '#weight' => 5,
    );
  }

  return $pane_form;
}

/**
 * Account pane: returns the username and e-mail for the Review checkout pane.
 */
function pab_pane_review($form, $form_state, $checkout_pane, $order) {
  $content = array();
  foreach($order->commerce_line_items[LANGUAGE_NONE] as $key => $line_item) {
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item['line_item_id']);
    if($line_item_wrapper->type->value() != 'product'){
      continue;
    }
    $content[] = array(
      '#type' => 'item',
      '#title' => t('Arrival Date'),
      '#markup' => date('l, F d, Y', $line_item_wrapper->field_start_date->value()),
    );

    $content[] = array(
      '#type' => 'item',
      '#title' => t('Departure Date'),
      '#markup' => date('l, F d, Y',$line_item_wrapper->field_end_date->value()),
    );

    $content[] = array(
      '#type' => 'item',
      '#title' => t('Number of Adults'),
      '#markup' => $line_item_wrapper->field_number_of_adults->value(),
    );

    $content[] = array(
      '#type' => 'item',
      '#title' => t('Pitch'),
      '#markup' => $line_item_wrapper->commerce_product->title->value(),
    );

    $value = field_get_items('commerce_order', $order, 'commerce_order_total');
    $price = field_view_value('commerce_order', $order, 'commerce_order_total', $value[0]);

    $value = field_get_items('commerce_order', $order, 'field_full_amount');
    $total = field_view_value('commerce_order', $order, 'field_full_amount', $value[0]);

    $content[] = array(
      '#type' => 'item',
      '#title' => t('Total Price'),
      '#markup' => render($total),
    );

    $content[] = array(
      '#type' => 'item',
      '#title' => t('Pay Today'),
      '#markup' => render($price),
    );
  }
  return drupal_render($content);
}
